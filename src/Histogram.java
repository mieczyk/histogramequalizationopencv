import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;

public class Histogram {
    private final Mat _inputImage;
    private Mat _equalizedImage;

    public Histogram(String imageFilePath) {
        _inputImage = Highgui.imread(imageFilePath);
        _equalizedImage = null;

        Imgproc.cvtColor(_inputImage, _inputImage, Imgproc.COLOR_BGR2GRAY);
    }

    public Image getInputImage() {
        return matToImage(_inputImage);
    }

    public Image getInputImageHistogram(int width, int height) {
       return getImageHistogram(_inputImage, width, height);
    }

    public Image getImageWithEqualizedHistogram() {
        if(_equalizedImage == null) {
            _equalizedImage = new Mat();
            Imgproc.equalizeHist(_inputImage, _equalizedImage);
        }

        return matToImage(_equalizedImage);
    }

    public Image getEqualizedHistogram(int width, int height) {
        if(_equalizedImage == null) {
            _equalizedImage = new Mat();
            Imgproc.equalizeHist(_inputImage, _equalizedImage);
        }

        return getImageHistogram(_equalizedImage, width, height);
    }

    private Image getImageHistogram(Mat image, int width, int height) {
        Mat histogram = new Mat();

        Imgproc.calcHist(
                Arrays.asList(image),
                new MatOfInt(0),
                new Mat(),
                histogram,
                new MatOfInt(256),
                new MatOfFloat(0f, 256f)
        );

        long binWidth = Math.round((double)(width / 256));

        Mat histImage = Mat.zeros(height, width, CvType.CV_8UC1);

        Core.normalize(histogram, histogram, 3, histImage.rows(), Core.NORM_MINMAX);

        for(int i = 1; i < 256; i++) {
            Point p1 = new Point(binWidth * (i - 1), height - histogram.get(i - 1, 0)[0]);
            Point p2 = new Point(binWidth * i, height - histogram.get(i, 0)[0]);

            Core.line(histImage, p1, p2, new Scalar(255, 255, 255), 2, Core.LINE_AA, 0);
        }

        return matToImage(histImage);
    }

    private Image matToImage(Mat img) {
        int bufferSize = img.channels() * img.cols() * img.rows();
        byte[] buffer = new byte[bufferSize];

        // get all pixels
        img.get(0, 0, buffer);

        BufferedImage resultImage = new BufferedImage(img.cols(), img.rows(), BufferedImage.TYPE_BYTE_GRAY);

        final byte[] targetPixels = ((DataBufferByte)resultImage.getRaster().getDataBuffer()).getData();

        System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);

        return resultImage;
    }
}
