import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class MainWindow extends JFrame {
    private static final int WIDTH = 650;
    private static final int HEIGHT = 800;
    private static final int PICTURE_WIDTH = 300;
    private static final int PICTURE_HEIGHT = 300;

    private JPanel _mainPanel;
    private JPanel _inputFilePanel;
    private JPanel _loadFilePanel;
    private JPanel _imagesPanel;
    private JPanel _inputImagePanel;
    private JPanel _histogramPanel;
    private JPanel _equalizedHistogramImagePanel;
    private JPanel _equalizedHistogramPanel;

    private JTextField _inputFileTextField;
    private JButton _selectFileButton;
    private JButton _loadFileButton;
    private JLabel _inputImageIconLabel;
    private JLabel _inputHistogramIconLabel;
    private JLabel _equalizedHistogramImageIconLabel;
    private JLabel _equalizedHistogramIconLabel;

    private File _lastDir;
    private Histogram _histogram;

    static {
        try {
            String libPath = new File(".").getCanonicalPath() + File.separator
                    + "lib" + File.separator
                    + "x64" + File.separator
                    + "opencv_java2411.dll";

            System.load(libPath);
        } catch(IOException ex) {
            System.err.println("Could not load OpenCV library!");
            System.exit(1);
        }
    }

    public MainWindow() {
        initComponents();
    }

    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainWindow main = new MainWindow();
                main.setVisible(true);
            }
        });
    }

    private void initComponents() {
        initPanels();

        _inputFilePanel.add(new JLabel("Select image:"), BorderLayout.NORTH);

        initInputFileTextField();
        initSelectFileButton();
        initLoadFileButton();

        _inputImagePanel.add(new JLabel("Input image:"), BorderLayout.NORTH);
        _inputImageIconLabel = new JLabel();
        _inputImagePanel.add(_inputImageIconLabel, BorderLayout.CENTER);

        _histogramPanel.add(new JLabel("Input image histogram:"), BorderLayout.NORTH);
        _inputHistogramIconLabel = new JLabel();
        _histogramPanel.add(_inputHistogramIconLabel, BorderLayout.CENTER);

        _equalizedHistogramImagePanel.add(new JLabel("Equalized histogram image:"), BorderLayout.NORTH);
        _equalizedHistogramImageIconLabel = new JLabel();
        _equalizedHistogramImagePanel.add(_equalizedHistogramImageIconLabel, BorderLayout.CENTER);

        _equalizedHistogramPanel.add(new JLabel("Equalized histogram:"), BorderLayout.NORTH);
        _equalizedHistogramIconLabel = new JLabel();
        _equalizedHistogramPanel.add(_equalizedHistogramIconLabel, BorderLayout.CENTER);

        add(_mainPanel);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("OpenCV Histogram Equalization");
        setSize(WIDTH, HEIGHT);
    }

    private void initPanels() {
        _mainPanel = new JPanel();
        _mainPanel.setLayout(new BoxLayout(_mainPanel, BoxLayout.PAGE_AXIS));

        _inputFilePanel = new JPanel();
        _inputFilePanel.setLayout(new BorderLayout());
        _inputFilePanel.setMaximumSize(new Dimension(WIDTH - 20, 100));
        _mainPanel.add(_inputFilePanel);

        _loadFilePanel = new JPanel();
        _loadFilePanel.setLayout(new BorderLayout());
        _loadFilePanel.setBorder(new EmptyBorder(10, 0, 10, 0));
        _loadFilePanel.setMaximumSize(new Dimension(WIDTH - 20, 100));
        _mainPanel.add(_loadFilePanel);

        _imagesPanel = new JPanel();
        _imagesPanel.setLayout(new GridLayout(2, 2));

        _inputImagePanel = new JPanel();
        _inputImagePanel.setLayout(new BorderLayout());
        _inputImagePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        _imagesPanel.add(_inputImagePanel);

        _histogramPanel = new JPanel();
        _histogramPanel.setLayout(new BorderLayout());
        _histogramPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        _imagesPanel.add(_histogramPanel);

        _equalizedHistogramImagePanel = new JPanel();
        _equalizedHistogramImagePanel.setLayout(new BorderLayout());
        _equalizedHistogramImagePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        _imagesPanel.add(_equalizedHistogramImagePanel);

        _equalizedHistogramPanel = new JPanel();
        _equalizedHistogramPanel.setLayout(new BorderLayout());
        _equalizedHistogramPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        _imagesPanel.add(_equalizedHistogramPanel);

        _mainPanel.add(_imagesPanel);
    }

    private void initInputFileTextField() {
        _inputFileTextField = new JTextField();

        _inputFileTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                _loadFileButton.setEnabled(true);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if("".equals(_inputFileTextField.getText())) {
                    _loadFileButton.setEnabled(false);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {}
        });

        _inputFilePanel.add(_inputFileTextField, BorderLayout.CENTER);
    }

    private void initSelectFileButton() {
        _selectFileButton = new JButton("...");

        _selectFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final FileFilter imageFilesFilter = new FileNameExtensionFilter(
                        "Image files (*.jpg, *.jpeg, *.png, *.bmp, *.tif)",
                        "jpg",
                        "jpeg",
                        "png",
                        "bmp",
                        "tif"
                );

                final JFileChooser fileChooser = new JFileChooser();

                fileChooser.addChoosableFileFilter(imageFilesFilter);
                fileChooser.setFileFilter(imageFilesFilter);
                fileChooser.setCurrentDirectory(_lastDir);

                int result = fileChooser.showOpenDialog(_mainPanel);

                if(result == JFileChooser.APPROVE_OPTION) {
                    _inputFileTextField.setText(fileChooser.getSelectedFile().getPath());

                    _lastDir = fileChooser.getCurrentDirectory();
                }
            }
        });

        _inputFilePanel.add(_selectFileButton, BorderLayout.EAST);
    }

    private void initLoadFileButton() {
        _loadFileButton = new JButton("Load file");

        _loadFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _histogram = new Histogram(_inputFileTextField.getText());

                int w = PICTURE_WIDTH;
                int h = PICTURE_HEIGHT;

                Image scaledInputImage = _histogram.getInputImage().getScaledInstance(w, h, Image.SCALE_SMOOTH);
                ImageIcon inputImageIcon = new ImageIcon(scaledInputImage);
                _inputImageIconLabel.setIcon(inputImageIcon);

                ImageIcon inputHistogramIcon = new ImageIcon(_histogram.getInputImageHistogram(w, h));
                _inputHistogramIconLabel.setIcon(inputHistogramIcon);

                Image scaledEqualizedImage = _histogram.getImageWithEqualizedHistogram().getScaledInstance(w, h, Image.SCALE_SMOOTH);
                ImageIcon equalizedImageIcon = new ImageIcon(scaledEqualizedImage);
                _equalizedHistogramImageIconLabel.setIcon(equalizedImageIcon);

                ImageIcon equalizedHistogramIcon = new ImageIcon(_histogram.getEqualizedHistogram(w, h));
                _equalizedHistogramIconLabel.setIcon(equalizedHistogramIcon);
            }
        });

        _loadFileButton.setEnabled(false);

        _loadFilePanel.add(_loadFileButton, BorderLayout.CENTER);
    }
}
